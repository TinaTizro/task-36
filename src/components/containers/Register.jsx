import React from 'react';
import RegisterForm from '../forms/RegisterForm';
import { Link, useHistory } from 'react-router-dom';


const Register = () => {
    const history = useHistory();

    const handleRegisterComplete = (result) => {
        console.log('Triggered from RegisterForm', result);
        if (result) {
            //redirect.
            history.replace("/dashboard");
        }

    };
    return (
        <div>
            <h1> Register for survey puppy</h1>
            <RegisterForm complete={handleRegisterComplete} />

            <Link to="/login">
                Already Registered? Login here!
        </Link>
        </div>
    )
};



export default Register;