import React from 'react';
import { Link } from 'react-router-dom';


const NotFound = props => { 
    return (
    <div>
        
        <h1>Page not found</h1>
        
        <Link to="/login">
        
            Go back to Login Page
            
                            
        </Link>
        <img src="https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdn.dribbble.com%2Fusers%2F1963449%2Fscreenshots%2F5915645%2F404_not_found.png" alt="Not Found" width="500" />   
        
    </div>
    
)
};

export default NotFound;