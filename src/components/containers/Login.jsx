import React  from 'react';
import LoginForm from '../forms/LoginForm';
import { Link } from 'react-router-dom';

const Login = () => (
    <div>
        <h1>Login to Survey puppy</h1> 
        <LoginForm />

        <Link to="/register">
            You have no account? Login here
        </Link>

    </div>
    );


export default Login;